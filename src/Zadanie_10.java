import java.math.BigDecimal;
import java.util.Scanner;

public class Zadanie_10 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите сумму кредита___");
        int sum = in.nextInt();
        System.out.print("Введите процентую годовую ставку___");
        double procent = in.nextInt();
        System.out.print("Введите срок кредитования (лет)___");
        int time = in.nextInt();
        double sum2 = sum * Math.pow((1 + (procent / 100)), time);//сумма кредита к оплате
        System.out.println("Сумма ежемесячного платежа " + new BigDecimal(sum2 / 12).setScale(2, BigDecimal.ROUND_HALF_UP));
        System.out.println("Сумма начисленная по % в месяц " + new BigDecimal(((sum / 12) - (sum2 / 12))).setScale(2, BigDecimal.ROUND_HALF_UP));
        System.out.println("Сумма выплаченных процентов " + new BigDecimal(sum - sum2).setScale(2, BigDecimal.ROUND_UP));
        System.out.println("Общая сумма кредита " + new BigDecimal(sum2).setScale(2, BigDecimal.ROUND_UP));

    }
}


