import java.util.Scanner;

public class Pz1 {
    public static void Zadanie_2() {
        float a = 1;
        float b = 1;
        float c = 0x1;
        float d = 0b1;
        double e = 1.0e1;
        float f = 01;
        System.out.println("\t a = " + a + "\t b = " + b + "\t c = " + c + "\t d = " + d + "\t e = " + e + "\t f = " + f);

    }

    public static void Zadanie_3() {
        short e = 8 + 1 / 4;
        short g = 2 + 5;
        System.out.println("\t e = " + e + "\t g = " + g);
    }

    public static void Zadanie_4() {
        boolean c;
        int katet_1 = 4;
        int katet_2 = 3;
        int gipotenyza = 5;
        if (katet_1 + katet_2 >= gipotenyza) {
            c = katet_1 * katet_1 + katet_2 * katet_2 == gipotenyza * gipotenyza;
            System.out.print(c ? "Прямоугольный" : "Не прямоугольный");
        } else {
            System.out.print("Invalid numbers. Try again.");
        }
    }

    public static void Zadanie_5() {
        int a = 0;
        for (int i = 1; i < 20 + 1; i++) {
            a = a + i;
        }
        System.out.println("a = " + a);
    }


    public static void Zadanie_6() {
        int a = 0;
        for (int i = 1; i < 20 + 1; i++) {
            if (i % 2 == 0)
                a = a + i;
        }

        System.out.println("a = " + a);
    }


    public static void Zadanie_7() {
        int s = 0;
        int m = 0;
        for (int i = 1; i < 20 + 1; i++) {
            for (int j = 1; j <= i; j++) {
                if (i % j == 0) {
                    m += 1;

                }
            }

            if (m == 2) {
                s += i;
                m = 0;
            } else {
                m = 0;
            }
        }
        System.out.println("s = " + s);
    }


    public static void Zadanie_8() {
        int a = 8;
        int b = 3;
        int c = 5;
        boolean d;
        if (a + b == c) {
            d = true;
        } else if (a + c == b) {
            d = true;
        } else if (b + c == a) {
            d = true;
        } else {
            d = false;
        }
        System.out.print(" d = " + d);
    }


    public static void Zadanie_9() {
        int d = 0;
        double e;
        int a = 4;
        int b = 1;
        double c = (a - b) + 1;
        if (a > b) {
            for (int i = b; i < a + 1; i++) {
                d += i;
            }
            e = d / c;
            System.out.print(" e = " + e);
        } else {
            System.out.print(" Invalid number ( a < b )");
        }
    }


}

